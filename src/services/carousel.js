export const getImages = async () => {
  return await fetch('https://run.mocky.io/v3/452b4fea-f89c-4aae-a090-0536c9058bdb')
    .then(data => data.json())
    .then(res => {
      const images = res.images || []
      return [ images, false ]
    })
    .catch(e => {
      return [ [], e ]
    })
}