import { defineStore } from 'pinia'
import { getImages } from '@/services/carousel'

export const useCarouselStore = defineStore({
  id: 'carousel',
  state: () => ({
    images: []
  }),
  actions: {
    async fetchImages() {
      const [ images, err ] = await getImages()
      if (!err) this.images = images

      return images
    }
  }
})
